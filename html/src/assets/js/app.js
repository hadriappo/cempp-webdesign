import $ from 'jquery';
import 'what-input';

// Foundation JS relies on a global varaible. In ES6, all imports are hoisted
// to the top of the file so if we used`import` to import Foundation,
// it would execute earlier than we have assigned the global variable.
// This is why we have to use CommonJS require() here since it doesn't
// have the hoisting behavior.
window.jQuery = $;
require('foundation-sites');

// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';

$(document).foundation();


/*toggle SHOP products*/

var front_product = $(".front");
var back_product = $(".back");
var switch_input = $(".switch-input");
var switch_paddle = $(".switch-paddle");

$(document).ready(function() {
	
	for (var i = 0; i < front_product.length; i++) {
		front_product[i].addEventListener('click', toggle_product, false);
		back_product[i].addEventListener('click', toggle_product, false);
		$(front_product[i]).attr("data-id", i); 
		$(back_product[i]).attr("data-id", i);
		$(switch_input[i]).attr("id", "switchquality"+i);
		$(switch_paddle[i]).attr("for", "switchquality"+i);
	}
});


function toggle_product(event) {
	 event.stopPropagation();
	
	var data_id=$(this).attr("data-id");
	
	if ($(this).hasClass("front")){
			
		
			for (var i = 0; i < front_product.length; i++) {
				$(front_product[i]).removeClass("hide-bar");
				$(back_product[i]).addClass("hide-bar");
			}
		
			$(this).toggleClass("hide-bar");
			$(back_product[data_id]).toggleClass("hide-bar");
			$('#right-bar-ads').addClass("hide-bar");
			if ($('#right-bar-product').hasClass("hide-bar")){
				$('#right-bar-product').toggleClass("hide-bar");
			}
	}	

};

function close_toggle(){
	if (!$(this).hasClass("front") && !$(this).hasClass("back")){
		for (var i = 0; i < front_product.length; i++) {
			$(front_product[i]).removeClass("hide-bar");
			$(back_product[i]).addClass("hide-bar");
		}
	}
	
};

document.addEventListener("click", close_toggle);

/*Custom Quantity Changer */

$(".quantity-changer").on("click", function() {

  var $button = $(this);
  var oldValue = $button.parent().find("input").val();

  if ($button.text() == "+") {
	  
		if($(this).hasClass("pref-quantity")){
			var newVal = parseFloat(oldValue) + 0.1;
		} else{
			var newVal = parseFloat(oldValue) + 1;
		}
	} else {
    if (oldValue > 0) {
		if($(this).hasClass("pref-quantity")){
			 var newVal = parseFloat(oldValue) - 0.1;
		} else{
			var newVal = parseFloat(oldValue) - 1;
		}
    } else {
      newVal = 0;
    }
  }

  $button.parent().find("input").val(newVal);

});



/*Custom select*/

/*var x, i, j, selElmnt, a, b, c;
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
 
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
 
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < selElmnt.length; j++) {

    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {

        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {

      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {

  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}

document.addEventListener("click", closeAllSelect);*/


/*Custom Inner Pages accordions */

var accordion_content = $(".accordion-inner-content");
var row = $(".item-row");
var container_row = $(".row-container");

$(document).ready(function(){

	for (var i = 0; i < accordion_content.length; i++) {
		$(accordion_content[i]).attr("id", "accordion-"+i);
		$(row[i]).attr("data-id", "accordion-"+i);
	}
  
	$('.item-cell').on('click', function(event){
		var accordion_data_id = $(this).parent().parent().attr("data-id");
		
		event.stopPropagation();
		
		for (var i = 0; i < accordion_content.length; i++) {
			var y = i-1
			if($(accordion_content[i]).attr("id") == accordion_data_id){
				if($('.item-cell').parent() !== $(this).parent() && $('.item-cell').parent().hasClass("hide")){
					$('.item-cell').parent().removeClass("hide");
				}
				$(this).parent().addClass("hide");

				$(accordion_content[i]).removeClass("hide");
				 
			}
			else{
				$(accordion_content[i]).addClass("hide");
			}
		}
		
		
	  
	});
  
});

function close_accordion(){
	if (!$(this).hasClass("item-cell") && !$(this).hasClass("accordion-inner-content")){
		for (var i = 0; i < accordion_content.length; i++) {
			$(accordion_content[i]).addClass("hide");
			if($('.item-cell').parent().hasClass("hide")){
				$('.item-cell').parent().removeClass("hide")
			}
		}
	}
	
};

document.addEventListener("click", close_accordion);

/*switch SAV */

$(document).ready(function(){
	$("input:checkbox#switch-sav").on('click', function(){
			for (var i = 0; i < row.length; i++) {
				if($(row[i]).hasClass("clos")){
					$(row[i]).toggleClass("hide");
				}
			}
	});
});


/*Toggle Portefeuille Page*/

$(document).ready(function(){
	
	var pf_dpt = $(".dpt");
	var pf_perso = $(".pf-perso");
	var pf_content = $(".pf-content");
	
	for(var i = 0; i < pf_dpt.length; i++){
		var dpt_text = $(pf_dpt[i]).text();
		$(pf_perso[i]).attr("data-id", dpt_text);
	}

	$("button.pf-button").on('click', function(){
		$("button.pf-button.button-top").removeClass("is-active");
		$(this).addClass("is-active");
		
		if($(this).is("#pf-client")){
			for(var i = 0; i < pf_content.length; i++){
				if($(pf_content[i]).is("#pf-client-content")){
					$(pf_content[i]).removeClass("hide");
					
				}
				else{
					$(pf_content[i]).addClass("hide");
				}
			}
		}
		else if($(this).is("#pf-departement")){
			for(var i = 0; i < pf_content.length; i++){
				if($(pf_content[i]).is("#pf-departement-content")){
					$(pf_content[i]).removeClass("hide");
				}
				else{
					$(pf_content[i]).addClass("hide");
				}
			}
		}
		else if($(this).is("#pf-secteur")){
			for(var i = 0; i < pf_content.length; i++){
				if($(pf_content[i]).is("#pf-secteur-content")){
					$(this).addClass("is-active");
					$(pf_content[i]).removeClass("hide");
				}
				else{
					$(pf_content[i]).addClass("hide");
				}
			}			
		}
	});
});


/*Toggle Compte Page */

$(document).ready(function(){
	var compte_content = $(".compte-content");
	
	$("button.compte-button").on('click', function(){
		$("button.compte-button").removeClass("is-active");
		$(this).addClass("is-active");
		
		if($(this).is("#compte-fiche")){
			for(var i = 0; i < compte_content.length; i++){
				if($(compte_content[i]).is("#fiche")){
					$(compte_content[i]).removeClass("hide");
					
				}
				else{
					$(compte_content[i]).addClass("hide");
				}
			}
		}
		else if($(this).is("#compte-preference")){
			for(var i = 0; i < compte_content.length; i++){
				if($(compte_content[i]).is("#preference")){
					$(compte_content[i]).removeClass("hide");
				}
				else{
					$(compte_content[i]).addClass("hide");
				}
			}
		}
	});
});

/*Display Command details Commande Page */

var detail_commande = $(".commande-detail");
var circle = $(".circle-container");
var card = $(".card-content");

$(document).ready(function(){

	for(var i = 0; i < detail_commande.length; i++){
		$(detail_commande[i]).attr("data-id", "detail-"+i);
		$(card[i]).attr("data-id", "detail-"+i);
	}

	$("div.card-content").on('click', function(){

		event.stopPropagation();
		
		var card_data_id = $(this).attr("data-id");
		for(var i = 0; i < detail_commande.length; i++){
			if ($(detail_commande[i]).attr("data-id")==card_data_id){
				$(detail_commande[i]).removeClass("hide");
				$(circle[i]).addClass("hide");
			}
			else{
				$(detail_commande[i]).addClass("hide");
				$(circle[i]).removeClass("hide");
			}
		}

	});
});

function close_commande_details(){
	if (!$(this).hasClass("circle-container") && !$(this).hasClass("commande-detail")){
		for (var i = 0; i < detail_commande.length; i++) {
			$(detail_commande[i]).addClass("hide");
			if($(circle[i]).hasClass("hide")){
				$(circle[i]).removeClass("hide")
			}
		}
	}
	
};

document.addEventListener("click", close_commande_details);